﻿using fourpm.Models;
using Microsoft.AspNetCore.Mvc;

namespace fourpm.Controllers
{
    public class newstudentController : Controller
    {
        
        //now i am creating action method
        public IActionResult find(int id)
        {
            student objname= new student();     

            //create student models
            if (id == 10)
            {
                objname.id = id;
                objname.name = "Abdulahi Jama Ali";
                objname.mobile = "252634458899";
                objname.email = "email@gmail.com";
                objname.classname = "2A ICT";
                objname.mother = "Xalimo";
            }else if (id == 2015)
            {
                objname.id = id;
                objname.name = "Ahmed Geedi";
                objname.mobile = "252634455566";
                objname.email = "ahmed@gmail.com";
                objname.classname = "2B Soft Engineering";
                objname.mother = "Amina";
            }


            return View(objname);
        }

        //this action will return all student 
        public IActionResult findall()
        {
            List<student> students= new List<student>();
            students.Add(new student() { id=10, name="Ali Jama", mobile="25263456633", email="email@gmail.com", classname="1A ICT", mother="Xaliimo"});
            students.Add(new student() { id = 11, name = "Ismail Jama", mobile = "252634455633", email = "email@gmail.com", classname = "1A ICT", mother = "Xaliimo" });
            students.Add(new student() { id = 12, name = "Ali Ismail", mobile = "252634456633", email = "email@gmail.com", classname = "12A ICT", mother = "Faadumo" });
            students.Add(new student() { id = 1130, name = "Jame Ali", mobile = "252634457633", email = "email@gmail.com", classname = "3A ICT", mother = "Amina" });

            return View(students);



        }
    }
}
