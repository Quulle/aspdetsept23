namespace fourpm
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddControllersWithViews();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();


            //your middle ware

            app.UseRouting();  // routing middle ware

            app.UseAuthorization();

            app.MapControllerRoute(
                name:"blognews",
                pattern:"news/{id}",
                defaults: new { controller="News", action="findnews"}


                );

            app.MapControllerRoute(
                name: "default",
                pattern: "{controller=Home}/{action=Index}/{id?}");

            app.Run();
        }
    }
}